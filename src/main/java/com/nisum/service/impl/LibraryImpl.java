package com.nisum.service.impl;

import com.nisum.domain.Book;
import com.nisum.enums.Genre;
import com.nisum.service.Library;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LibraryImpl implements Library {

    Map<Genre, LinkedList<Book>> libraryByGenre;

    public LibraryImpl() {
        libraryByGenre = new HashMap<>();
        libraryByGenre.put(Genre.WESTERN, new LinkedList<Book>());
        libraryByGenre.put(Genre.GENERAL_FICTION, new LinkedList<Book>());
        libraryByGenre.put(Genre.NON_FICTION, new LinkedList<Book>());
        libraryByGenre.put(Genre.SCIENCE_FICTION, new LinkedList<Book>());
    }

    @Override
    public Book checkOutBook(Genre genre) throws OutOfBooksException {
        LinkedList<Book> booksByGenre = libraryByGenre.get(genre);
        if (booksByGenre.isEmpty()) {
            throw new OutOfBooksException();
        }

        Book desiredBook = booksByGenre.getFirst();
        libraryByGenre.get(genre).removeFirst();
        return desiredBook;
    }

    @Override
    public void checkInBook(Book returnedBook, int rating) throws IllegalRatingException {
        if (rating > 100 || rating < 1) {
            throw new IllegalRatingException();
        }

        Book newReturnedBook = new Book(returnedBook.getGenre(), returnedBook.getTitle(), returnedBook.getAuthor(), rating);
        libraryByGenre.get(newReturnedBook.getGenre()).addFirst(newReturnedBook);
    }

    @Override
    public Book peekHighestRatedBook(Genre genre) throws OutOfBooksException {
        LinkedList<Book> booksBySelectedGenre = libraryByGenre.get(genre);
        if (booksBySelectedGenre.isEmpty()) {
            throw new OutOfBooksException();
        }

        Book controlBook = booksBySelectedGenre.getFirst();
        for (Book book : booksBySelectedGenre) {
            if (book.getRating() > controlBook.getRating()) {
                controlBook = book;
            }
        }
        return controlBook;
    }
}

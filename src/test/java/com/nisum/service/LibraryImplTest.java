package com.nisum.service;

import com.nisum.domain.Book;
import com.nisum.enums.Genre;
import com.nisum.service.impl.LibraryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by smurua on 01-09-2015.
 */
public class LibraryImplTest {

    private LibraryImpl instance;
    private LinkedList<Book> generalFictionBooks;
    private Map<Genre, LinkedList<Book>> libraryByGenres;

    @Before
    public void setUp() throws Exception {
        instance = new LibraryImpl();

        Book book = new Book(Genre.GENERAL_FICTION, "Title", "Author", 10);

        Field field = LibraryImpl.class.getDeclaredField("libraryByGenre");
        field.setAccessible(true);

        libraryByGenres = (Map<Genre, LinkedList<Book>>) field.get(instance);

        generalFictionBooks = libraryByGenres.get(Genre.GENERAL_FICTION);
        generalFictionBooks.add(book);
    }

    @Test
    public void checkInBook_withValidBookAndRatingAndGenre_addTheBookToTheLibraryAndUpdateTheRating() throws Exception {
        //setUp
        int rating = 100;
        int previousRating = 33;
        Book returnedBook = new Book(Genre.GENERAL_FICTION, "NewBook", "NewAuthor", previousRating);

        //exercise
        instance.checkInBook(returnedBook, rating);

        //postSetUp
        Book newReturnedBook = generalFictionBooks.getFirst();

        //verify
        Assert.assertEquals(2, generalFictionBooks.size());
        Assert.assertEquals(returnedBook.getGenre(), newReturnedBook.getGenre());
        Assert.assertEquals(returnedBook.getTitle(), newReturnedBook.getTitle());
        Assert.assertEquals(returnedBook.getAuthor(), newReturnedBook.getAuthor());
        Assert.assertNotEquals(previousRating, newReturnedBook.getRating());
        Assert.assertEquals(rating, newReturnedBook.getRating());
    }

    @Test(expected = Library.IllegalRatingException.class)
    public void checkInBook_withValidBookAnHighInvalidRating_throwsIllegalRatingException() throws Exception {
        //setUp
        int highInvalidRating = 101;
        int previousRating = 33;
        Book returnedBook = new Book(Genre.WESTERN, "WesternBook", "WesternAuthor", previousRating);

        //exercise
        instance.checkInBook(returnedBook, highInvalidRating);

        //verify
        Assert.fail("The method didn't throw the expected exception");
    }

    @Test(expected = Library.IllegalRatingException.class)
    public void checkInBook_withValidBookAnLowInvalidRating_throwsIllegalRatingException() throws Exception {
        //setUp
        int highInvalidRating = 0;
        int previousRating = 33;
        Book returnedBook = new Book(Genre.WESTERN, "WesternBook", "WesternAuthor", previousRating);

        //exercise
        instance.checkInBook(returnedBook, highInvalidRating);

        //verify
        Assert.fail("The method didn't throw the expected exception");
    }

    @Test
    public void checkOutBook_withGenreWithBooks_returnTheMostRecentlyCheckedIn() throws Exception {
        //setUp
        Genre desiredGenre = Genre.GENERAL_FICTION;
        Book expectedBook = new Book(desiredGenre, "TheMosRecentlyCheckedIn", "Myself", 30);
        generalFictionBooks.addFirst(expectedBook);
        int generalFictionBooksSize = generalFictionBooks.size();

        //exercise
        Book result = instance.checkOutBook(desiredGenre);

        //verify
        Assert.assertSame(expectedBook, result);
        Assert.assertEquals(generalFictionBooksSize - 1, generalFictionBooks.size());
    }

    @Test(expected = Library.OutOfBooksException.class)
    public void checkOutBook_withGenreWithoutBooks_throwsOutOfBooksException() throws Exception {
        //setUp
        Genre desiredGenre = Genre.NON_FICTION;

        //exercise
        instance.checkOutBook(desiredGenre);

        //verify
        Assert.fail("The method didn't throw the expected exception");
    }

    @Test
    public void peekHighestRatedBook_withThreeBookWithTheSameGenre_returnTheHighestRated() throws Exception {
        //setUp
        Genre selectedGenre = Genre.NON_FICTION;

        Book reallyBoringBook = new Book(selectedGenre, "ReallyBoringBook", "ReallyBoringAuthor", 10);
        Book awesomeBook = new Book(selectedGenre, "AwesomeBook", "AwesomeAuthor", 90);
        Book coolBook = new Book(selectedGenre, "CoolBook", "CoolAuthor", 70);

        LinkedList<Book> selectedGenreBooks = libraryByGenres.get(selectedGenre);
        selectedGenreBooks.clear();
        selectedGenreBooks.add(reallyBoringBook);
        selectedGenreBooks.add(awesomeBook);
        selectedGenreBooks.add(coolBook);

        int quantityBeforePeek = selectedGenreBooks.size();

        //exercise
        Book result = instance.peekHighestRatedBook(selectedGenre);

        //verify
        Assert.assertSame(awesomeBook, result);
        Assert.assertEquals(quantityBeforePeek, libraryByGenres.get(selectedGenre).size());
    }

    @Test(expected = Library.OutOfBooksException.class)
    public void peekHighestRatedBook_withGenreWithoutBooks_throwsOutOfBooksException() throws Exception {
        //setUp
        Genre desiredGenre = Genre.NON_FICTION;

        //exercise
        instance.peekHighestRatedBook(desiredGenre);

        //verify
        Assert.fail("The method didn't throw the expected exception");
    }


}

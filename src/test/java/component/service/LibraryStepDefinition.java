package component.service;

import com.nisum.domain.Book;
import com.nisum.enums.Genre;
import com.nisum.service.Library;
import com.nisum.service.impl.LibraryImpl;
import component.BaseStepDefinition;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import sun.security.util.PendingException;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LibraryStepDefinition extends BaseStepDefinition {

    @Before
    public void setUp() throws Exception {
        instance = new LibraryImpl();
        Field field = LibraryImpl.class.getDeclaredField("libraryByGenre");
        field.setAccessible(true);

        libraryByGenres = (Map<Genre, LinkedList<Book>>) field.get(instance);
    }

    //CheckInBook
    @Given("^I have a book with title '(.+)', author '(.+)', genre '(.+)' and a previous rating of '(\\d+)'$")
    public void i_have_a_book_with_title_author_and_genre(String title, String author, Genre genre, int previousRating) throws Throwable {
        Book randomBookCheckedInBefore = new Book(genre, "RandomBook", "RandomAuthor", 30);
        libraryByGenres.get(genre).addFirst(randomBookCheckedInBefore);
        stepDefinitionContext.put("newBook", new Book(genre, title, author, previousRating));
    }

    @When("^I check in my book along with a rating of '(\\d+)'$")
    public void i_check_in_my_book_along_with_a_rating_of(int newRating) throws Throwable {
        stepDefinitionContext.put("newRating", newRating);
        try {
            instance.checkInBook((Book) stepDefinitionContext.get("newBook"), newRating);
        } catch (Exception exception) {
            stepDefinitionContext.put("resultException", exception);
        }
    }

    @Then("^The book should be added to the library with my rating$")
    public void the_book_should_be_added_to_the_library_with_my_rating() throws Throwable {
        Book newBook = (Book) stepDefinitionContext.get("newBook");
        Book firstBookBygGenre = libraryByGenres.get(newBook.getGenre()).getFirst();
        Assert.assertEquals(newBook.getGenre(), firstBookBygGenre.getGenre());
        Assert.assertEquals(newBook.getTitle(), firstBookBygGenre.getTitle());
        Assert.assertEquals(newBook.getAuthor(), firstBookBygGenre.getAuthor());
        Assert.assertNotEquals(newBook.getRating(), firstBookBygGenre.getRating());
        Assert.assertEquals(stepDefinitionContext.get("newRating"), firstBookBygGenre.getRating());
    }

    @Then("^I should see an IllegalRatingException$")
    public void i_should_see_an_IllegalRatingException() throws Throwable {
        Exception exception = (Exception) stepDefinitionContext.get("resultException");
        Assert.assertNotNull("The method didn't throw an exception", exception);
        Assert.assertTrue("It wasn't the expected exception", exception instanceof Library.IllegalRatingException);
    }

    //CheckOutBook
    @Given("^I have a the following books available(?:, the first book in the list is the most recently checked in|):$")
    public void i_have_a_the_following_books_available_the_first_book_in_the_list_is_the_most_recently_checked_in(List<Map<String, String>> books) throws Throwable {
        for (Map<String, String> dataBoook : books) {
            Genre bookGenre = Genre.valueOf(dataBoook.get("genre"));
            Book book = new Book(bookGenre, dataBoook.get("title"), dataBoook.get("author"), Integer.parseInt(dataBoook.get("rating")));
            libraryByGenres.get(bookGenre).addLast(book);
        }
    }

    @When("^I check out a book with the genre '(.+)'$")
    public void i_check_out_a_book_with_the_genre(Genre genre) throws Throwable {
        try {
            Book result = instance.checkOutBook(genre);
            stepDefinitionContext.put("resultBook", result);
        } catch (Exception exception) {
            stepDefinitionContext.put("resultException", exception);
        }
    }

    @Then("^I should get the book with the title '(.+)'$")
    public void i_should_get_the_book_with_the_title_Advanced_Spring(String bookTitle) throws Throwable {
        Book resultBook = (Book) stepDefinitionContext.get("resultBook");
        Assert.assertEquals(bookTitle, resultBook.getTitle());
    }


    @Then("^I should see an OutOfBooksException$")
    public void i_should_see_an_OutOfBooksException() throws Throwable {
        Exception exception = (Exception) stepDefinitionContext.get("resultException");
        Assert.assertNotNull("The method didn't throw an exception", exception);
        Assert.assertTrue("It wasn't the expected exception", exception instanceof Library.OutOfBooksException);
    }

    //PeekHighestRatedBook
    @When("^I peek the highest rated with the genre '(.+)'$")
    public void i_peek_the_highest_rated_with_the_genre(Genre genre) throws Throwable {
        try {
            stepDefinitionContext.put("resultBook", instance.peekHighestRatedBook(genre));
        } catch (Exception exception) {
            stepDefinitionContext.put("resultException", exception);
        }
    }

}

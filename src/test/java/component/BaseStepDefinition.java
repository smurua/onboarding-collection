package component;

import com.nisum.domain.Book;
import com.nisum.enums.Genre;
import com.nisum.service.impl.LibraryImpl;
import cucumber.api.java.Before;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class BaseStepDefinition {

    protected final Map<String, Object> stepDefinitionContext = new HashMap<>();

    protected LibraryImpl instance;
    protected Map<Genre, LinkedList<Book>> libraryByGenres;


}

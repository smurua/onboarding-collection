Feature: Check out the most recently checked in

  Scenario: Check out a book by genre available
    Given I have a the following books available, the first book in the list is the most recently checked in:
      | title                                | author             | rating | genre           |
      | Lonesome Dove                        | Larry McMurtry     | 10     | WESTERN         |
      | Advanced Spring                      | John Sprouts       | 83     | NON_FICTION     |
      | Sherlock Holmes                      | Arthur Conan Doyle | 100    | GENERAL_FICTION |
      | The Hitchhiker's Guide to the Galaxy | Douglas Adams      | 90     | SCIENCE_FICTION |
      | Hello World                          | Mr Java            | 33     | NON_FICTION     |
    When I check out a book with the genre 'NON_FICTION'
    Then I should get the book with the title 'Advanced Spring'

  Scenario: Check out a book by genre unavailable
    Given I have a the following books available, the first book in the list is the most recently checked in:
      | title           | author       | rating | genre       |
      | Advanced Spring | John Sprouts | 83     | NON_FICTION |
      | Hello World     | Mr Java      | 33     | NON_FICTION |
    When I check out a book with the genre 'GENERAL_FICTION'
    Then I should see an OutOfBooksException
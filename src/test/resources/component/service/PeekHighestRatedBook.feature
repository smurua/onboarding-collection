Feature: Peek the highest rated book in the specified genre, but does not remove it from availability

  Scenario: Peek the highest rated book in the specified genre
    Given I have a the following books available:
      | title                                | author             | rating | genre       |
      | Lonesome Dove                        | Larry McMurtry     | 10     | NON_FICTION |
      | Advanced Spring                      | John Sprouts       | 83     | NON_FICTION |
      | Sherlock Holmes                      | Arthur Conan Doyle | 100    | NON_FICTION |
      | The Hitchhiker's Guide to the Galaxy | Douglas Adams      | 90     | NON_FICTION |
      | Hello World                          | Mr Java            | 33     | NON_FICTION |
    When I peek the highest rated with the genre 'NON_FICTION'
    Then I should get the book with the title 'Sherlock Holmes'

  Scenario: Peek the highest rated book with a genre without books
    Given I have a the following books available, the first book in the list is the most recently checked in:
      | title           | author       | rating | genre       |
      | Advanced Spring | John Sprouts | 83     | NON_FICTION |
      | Hello World     | Mr Java      | 33     | NON_FICTION |
    When I peek the highest rated with the genre 'GENERAL_FICTION'
    Then I should see an OutOfBooksException
Feature: Check in a book and override user rating

  Scenario: Check in a book along with a valid rating
    Given I have a book with title 'Java Collections', author 'Some Guy', genre 'NON_FICTION' and a previous rating of '30'
    When I check in my book along with a rating of '100'
    Then The book should be added to the library with my rating

  Scenario: Check in a book along with an rating higher than 100
    Given I have a book with title 'Java Collections', author 'Some Guy', genre 'NON_FICTION' and a previous rating of '30'
    When I check in my book along with a rating of '101'
    Then I should see an IllegalRatingException

  Scenario: Check in a book along with an rating lower than 1
    Given I have a book with title 'Java Collections', author 'Some Guy', genre 'NON_FICTION' and a previous rating of '30'
    When I check in my book along with a rating of '0'
    Then I should see an IllegalRatingException